import request from '@/utils/request'

// 校区到访率
export function fetchCampusVisitCon(query) {
  return request({
    url: '/business/statistic/campusVisit',
    method: 'get',
    params: query
  })
}

export function fetchExportCampusVisitCon(query) {
  return request({
    url: '/business/statistic-export/campusVisit',
    method: 'get',
    params: query
  })
}




// 校区到访转成交率
export function fetchCampusVisitRateCon(query) {
  return request({
    url: '/business/statistic/campusVisitRate',
    method: 'get',
    params: query
  })
}


export function fetchExportCampusVisitRateCon(query) {
  return request({
    url: '/business/statistic-export/campusVisitRate',
    method: 'get',
    params: query
  })
}




// 用户到访率
export function fetchUserVisitCon(query) {
  return request({
    url: '/business/statistic/userVisit',
    method: 'get',
    params: query
  })
}

export function fetchExportUserVisitCon(query) {
  return request({
    url: '/business/statistic-export/userVisit',
    method: 'get',
    params: query
  })
}


// 用户到访转成交率
export function fetchUserVisitRateCon(query) {
  return request({
    url: '/business/statistic/userVisitRate',
    method: 'get',
    params: query
  })
}

export function fetchExportUserVisitRateCon(query) {
  return request({
    url: '/business/statistic-export/userVisitRate',
    method: 'get',
    params: query
  })
}


// 老师试听报名率
export function fetchTeacherEnrollRateCon(query) {
  return request({
    url: '/business/statistic/teacherEnrollRate',
    method: 'get',
    params: query
  })
}

export function fetchExportTeacherEnrollRateCon(query) {
  return request({
    url: '/business/statistic-export/teacherEnrollRate',
    method: 'get',
    params: query
  })
}

// 业绩排行
export function fetchPerformanceRank(query) {
  return request({
    url: '/business/statistic/performanceRanking',
    method: 'get',
    params: query
  })
}

export function fetchExportPerformanceRank(query) {
  return request({
    url: '/business/statistic-export/performanceRanking',
    method: 'get',
    params: query
  })
}
