import request from '@/utils/request'

//获取所有校区()
export function fetchOrg(queryMethod,queryKey) {
  return request({
    url: '/business/query-select/organization',
    method: 'get',
    params: {queryMethod,queryKey}
  })
}

//根据校区获取所有渠道(校区ID)
export function fetchChannel(queryId,queryKey) {

  return request({
    url: '/business/query-select/channel',
    method: 'get',
    params: {queryId,queryKey}
  })
}

//课程（校区Id）
export function fetchCourse(queryId,queryKey) {
  return request({
    url: '/business/query-select/course',
    method: 'get',
    params: {queryId,queryKey}
  })
}

//老师(课程ID,校区ID)
export function fetchTeahcer(courseId, orgId) {
  return request({
    url: '/business/query-select/teacher',
    method: 'get',
    params: {courseId,orgId}
  })
}

//咨询师(校区ID)
export function fetchOwner(orgId) {
  return request({
    url: '/business/query-select/user',
    method: 'get',
    params: { orgId }
  })
}

//新增线索
export function createClue(data) {
  return request({
    url: '/business/clue/create',
    method: 'post',
    data
  })
}

//查询单个线索
export function fetchClue(clueId) {
  return request({
    url: '/business/clue/info',
    method: 'get',
    params: { clueId }
  })
}

//分页查询线索
export function fetchList(query) {
  return request({
    url: '/business/clue/list',
    method: 'get',
    params: query
  })
}

//分页查询线索
export function fetchExportList(query) {
  return request({
    url: '/business/clue/export',
    method: 'get',
    params: query
  })
}


//编辑线索
export function updateClue(data) {
  return request({
    url: '/business/clue/update',
    method: 'post',
    data
  })
}


//分页查询线索
export function fetchRemindList(query) {
  return request({
    url: '/business/clue/notifyList',
    method: 'get',
    params: query
  })
}

export function deleteClue(id) {
  return request({
    url: '/business/clue/delete/' + id,
    method: 'post'
  })
}
