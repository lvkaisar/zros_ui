import request from '@/utils/request'

export function fetchDataResourceList(data) {
  return request({
    url: '/dataresource/tree',
    method: 'get',
    params: data
  })
}


/*以下方法暂时不用，所以命名还是原来的*/
export function createResource(data) {
  return request({
    url: '/resource/create',
    method: 'post',
    data
  })
}

export function updateResource(data) {
  return request({
    url: '/resource/update',
    method: 'post',
    data
  })
}

export function deleteResource(id) {
  return request({
    url: '/resource/delete/' + id,
    method: 'post'
  })
}
