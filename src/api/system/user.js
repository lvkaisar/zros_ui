import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}

export function createUser(data) {
  return request({
    url: '/user/create',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data
  })
}

export function updateUserInfo(data) {
  return request({
    url: '/user/update/info',
    method: 'post',
    data
  })
}

export function queryUserInfo(data) {
  return request({
    url: '/auth/user/info',
    method: 'get',
    data
  })
}

export function updatePwd(data) {
  return request({
    url: '/user/pwd',
    method: 'post',
    data
  })
}

export function resetPwd(userId) {
  return request({
    url: '/user/password/' + userId + '/',
    method: 'post'
  })
}


export function updateUserStatus(userId, status) {
  return request({
    url: '/user/status/' + userId + '/' + status,
    method: 'post'
  })
}

export function deleteUser(id) {
  return request({
    url: '/user/delete/' + id,
    method: 'post'
  })
}

export function fetchRoleList(data) {
  return request({
    url: '/role/all',
    method: 'get',
    data
  })
}

//获取所有校区()
export function fetchOrg() {
  return request({
    url: '/business/query-select/allOrganization',
    method: 'get'
  })
}
