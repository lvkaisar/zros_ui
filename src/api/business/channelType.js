import request from '@/utils/request'

// 分页查询
export function fetchCtList(query) {
  return request({
    url: '/business/channel-type/list',
    method: 'get',
    params: query
  })
}

export function createCt(data) {
  return request({
    url: '/business/channel-type/create',
    method: 'post',
    data
  })
}

export function updateCt(data) {
  return request({
    url: '/business/channel-type/update',
    method: 'post',
    data
  })
}

export function deleteCt(id) {
  return request({
    url: '/business/channel-type/delete/' + id,
    method: 'post'
  })
}
