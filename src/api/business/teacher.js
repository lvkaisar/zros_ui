import request from '@/utils/request'

// 分页查询
export function fetchTeacherList(query) {
  return request({
    url: '/business/teacher/list',
    method: 'get',
    params: query
  })
}

export function createTeacher(data) {
  return request({
    url: '/business/teacher/create',
    method: 'post',
    data
  })
}

export function updateTeacher(data) {
  return request({
    url: '/business/teacher/update',
    method: 'post',
    data
  })
}

export function deleteTeacher (id) {
  return request({
    url: '/business/teacher/delete/' + id,
    method: 'post'
  })
}
