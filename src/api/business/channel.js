import request from '@/utils/request'

// 分页查询
export function fetchChannelList(query) {
  return request({
    url: '/business/channel/list',
    method: 'get',
    params: query
  })
}

export function createChannel(data) {
  return request({
    url: '/business/channel/create',
    method: 'post',
    data
  })
}

export function updateChannel(data) {
  return request({
    url: '/business/channel/update',
    method: 'post',
    data
  })
}

export function deleteChannel(id) {
  return request({
    url: '/business/channel/delete/' + id,
    method: 'post'
  })
}
