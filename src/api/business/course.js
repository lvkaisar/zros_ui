import request from '@/utils/request'

// 分页查询
export function fetchCourseList(query) {
  return request({
    url: '/business/course/list',
    method: 'get',
    params: query
  })
}

export function createCourse(data) {
  return request({
    url: '/business/course/create',
    method: 'post',
    data
  })
}

export function updateCourse(data) {
  return request({
    url: '/business/course/update',
    method: 'post',
    data
  })
}

export function deleteCourse (id) {
  return request({
    url: '/business/course/delete/' + id,
    method: 'post'
  })
}
