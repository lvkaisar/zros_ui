import request from '@/utils/request'

// 分页查询
export function fetchDepartmentList(query) {
  return request({
    url: '/business/department/list',
    method: 'get',
    params: query
  })
}

export function createDepartment(data) {
  return request({
    url: '/business/department/create',
    method: 'post',
    data
  })
}

export function updateDepartment(data) {
  return request({
    url: '/business/department/update',
    method: 'post',
    data
  })
}

export function deleteDepartment (id) {
  return request({
    url: '/business/department/delete/' + id,
    method: 'post'
  })
}
