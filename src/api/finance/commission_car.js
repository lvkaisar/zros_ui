import request from '@/utils/request'

export function fetchCommissionCarList(query) {
  return request({
    url: '/finance/commission/list',
    method: 'get',
    params: query
  })
}

export function createCommissionCar(data) {
  return request({
    url: '/finance/commission/create',
    method: 'post',
    data
  })
}

export function updateCommissionCar(data) {
  return request({
    url: '/finance/commission/update',
    method: 'post',
    data
  })
}

export function updateCommissionCarInfo(data) {
  return request({
    url: '/finance/commission/update/info',
    method: 'post',
    data
  })
}

export function updateCommissionCarStatus(commissionCarId, status) {
  return request({
    url: '/finance/commission/status/' + commissionCarId + '/' + status,
    method: 'post'
  })
}

export function deleteCommissionCar(id) {
  return request({
    url: '/finance/commission/delete/' + id,
    method: 'post'
  })
}
