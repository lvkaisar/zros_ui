import request from '@/utils/request'

export function fetchCommissionShareList(query) {
  return request({
    url: '/finance/commission/share/list',
    method: 'get',
    params: query
  })
}

export function createCommissionShare(data) {
  return request({
    url: '/finance/commission/share/create',
    method: 'post',
    data
  })
}

export function updateCommissionShare(data) {
  return request({
    url: '/finance/commission/share/update',
    method: 'post',
    data
  })
}

export function updateCommissionShareStatus(commissionId, status) {
  return request({
    url: '/finance/commission/share/status/' + commissionId + '/' + status,
    method: 'post'
  })
}

export function deleteCommissionShare(id) {
  return request({
    url: '/finance/commission/share/delete/' + id,
    method: 'post'
  })
}
