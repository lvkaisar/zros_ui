
import {
  fetchRemindList
} from "@/api/clue";
const remindClue = {
  state: {
    isShowDialog: true,
    remindClues: []
  },

  mutations: {
    SET_IS_SHOW_DIALOG: (state, code) => {
      state.isShowDialog = code
    },
    SET_REMIND_CLUES: (state, token) => {
      state.remindClues = token
    }
  },

  actions: {
    // 用户名登录
    QueryRemindClue({ commit }, clueInfo) {

      return new Promise((resolve, reject) => {
        fetchRemindList().then(response => {
          const remindList = response.data
          commit('SET_REMIND_CLUES', remindList)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 用户名登录
    UpdateIsShowDialog({ commit }, clueInfo) {
      commit('SET_IS_SHOW_DIALOG', false)
    },
  }
}

export default remindClue
